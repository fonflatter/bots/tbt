const fs = require('fs')
const moment = require('moment')

const comic = JSON.parse(fs.readFileSync('comic.json'))[0]
const date = `${comic.date_gmt}Z`
const comicUrl = comic.link
const imageUrl = comic.comic_image_url

const daysSinceComic = moment().diff(date, 'days')

const payload = {
  text: `Heute vor ${daysSinceComic} Tagen erschien dieser fetzige Comic. #TBT\n\n${comicUrl}`,
  mediaUrls: [imageUrl]
}

fs.writeFileSync('payload.json', JSON.stringify(payload))
