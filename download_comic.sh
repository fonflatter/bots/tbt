#!/usr/bin/env sh

set -o errexit
set -o nounset

TBT_DATE=$1

wget --output-document=comic.json "https://fonflatter.de/wp-json/wp/v2/posts/?tbt=${TBT_DATE}&per_page=1"
